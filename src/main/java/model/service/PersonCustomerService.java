package model.service;

import model.entity.PersonCustomer;
import model.repository.PersonCustomerDA;

import java.util.List;

/**
 * Created by Hi on 8/1/2020.
 */
public class PersonCustomerService {
    public static void save(PersonCustomer person) throws Exception {
        System.out.println(person.getName()+""+person.getId());
        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
        personCustomerDA.insert(person);
        personCustomerDA.close();
    }
    public  String showCustomerNumber()throws Exception
    {
        PersonCustomerDA personCustomerDA=new PersonCustomerDA();
        String customerNumber=personCustomerDA.selectCustomerNumber();
        personCustomerDA.close();
        return customerNumber;
    }
    public static List<PersonCustomer> findAll()throws Exception
    {
        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
        List<PersonCustomer> personList = personCustomerDA.select();
        personCustomerDA.close();
        return personList;
    }

    public static List<PersonCustomer>searchByName(String name)throws Exception
    {
        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
        List<PersonCustomer> personList = personCustomerDA.searchByName(name);
        personCustomerDA.close();
        return personList;
    }
    public static List<PersonCustomer>searchByLastName(String lastname)throws Exception
    {
        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
        List<PersonCustomer> personList = personCustomerDA.searchByLastName(lastname);
        personCustomerDA.close();
        return personList;
    }

    public static List<PersonCustomer>searchByIdentityNumber(String identityNumber )throws Exception
    {
        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
        List<PersonCustomer> personList = personCustomerDA.searchByIdentityNumber(identityNumber);
        personCustomerDA.close();
        return personList;
    }
    public static List<PersonCustomer>searchById(int id)throws Exception
    {
        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
        List<PersonCustomer> personList = personCustomerDA.searchById(id);
        personCustomerDA.close();
        return personList;
    }
    public void update(PersonCustomer person) throws Exception {
        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
        System.out.println("in service"+person.getName()+"id:"+person.getId());
        personCustomerDA.update(person);
        personCustomerDA.close();
    }
    public static void delete(int id) throws Exception {
        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
        personCustomerDA.delete(id);
        personCustomerDA.close();
    }
}

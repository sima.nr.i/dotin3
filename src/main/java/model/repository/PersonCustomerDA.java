package model.repository;

import model.entity.PersonCustomer;

import java.sql.*;
import java.util.*;

/**
 * Created by Hi on 8/1/2020.
 */
public class PersonCustomerDA {
    Random random = new Random();
    private PreparedStatement preparedStatement;
    private int countCustomers = 0;
    private Connection connection;
    public static List<Integer> customernumbers = new ArrayList<Integer>();

    public PersonCustomerDA() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        connection = DriverManager.getConnection("jdbc:mysql://localhost/dotin3?" + "user=sima&password=myjava123");

    }

    public void insert(PersonCustomer personCustomer) throws Exception {

        preparedStatement = connection.prepareStatement("insert into personcustomer" +
                " (customernumber,name,lastname,identitynumber,nameoffather,dateofbirth)" +
                " values (?,?,?,?,?,?)");
        countCustomers++;
        for (int i = 100; i < 1000; i++) {
            customernumbers.add(new Integer(i));
        }
        Collections.shuffle(customernumbers);
        preparedStatement.setInt(1, (customernumbers.get(countCustomers)));
        preparedStatement.setString(2, personCustomer.getName());
        System.out.println(personCustomer.getLastName());
        preparedStatement.setString(3, personCustomer.getLastName());
        preparedStatement.setString(4, personCustomer.getIdentityNumber());
        preparedStatement.setString(5, personCustomer.getNameOfFather());
        preparedStatement.setString(6, personCustomer.getDateOfBirth());
        preparedStatement.executeUpdate();
    }

    public String selectCustomerNumber() throws Exception {
        preparedStatement = connection.prepareStatement("SELECT customernumber FROM dotin3.personcustomer ORDER BY id DESC LIMIT 0, 1\n");
        ResultSet resultSet = preparedStatement.executeQuery();
        String customerNumber = null;
        while (resultSet.next()) {
            customerNumber = resultSet.getString("customernumber");
        }
        return customerNumber;
    }

    public List<PersonCustomer> select() throws Exception {
        preparedStatement = connection.prepareStatement("select * from personcustomer ");
        ResultSet resultSet = preparedStatement.executeQuery();
        List<PersonCustomer> personList = new ArrayList<PersonCustomer>();
        while (resultSet.next()) {
            PersonCustomer person = new PersonCustomer(
                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("name"),
                    resultSet.getString("lastName"), resultSet.getString("identitynumber"),
                    resultSet.getString("nameoffather"), resultSet.getString("dateofbirth"));
            personList.add(person);
        }
        return personList;
    }

    public List<PersonCustomer> searchByName(String name) throws Exception {
        preparedStatement = connection.prepareStatement("select * from personcustomer WHERE NAME =?");
        preparedStatement.setString(1, name);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<PersonCustomer> personList = new ArrayList<PersonCustomer>();
        while (resultSet.next()) {
            PersonCustomer person = new PersonCustomer(
                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("name"),
                    resultSet.getString("lastName"), resultSet.getString("identitynumber"),
                    resultSet.getString("nameoffather"), resultSet.getString("dateofbirth"));
            personList.add(person);
        }
        return personList;
    }

    public List<PersonCustomer> searchByLastName(String lastname) throws Exception {
        preparedStatement = connection.prepareStatement("select * from personcustomer WHERE lastname=?");
        preparedStatement.setString(1, lastname);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<PersonCustomer> personList = new ArrayList<PersonCustomer>();
        while (resultSet.next()) {
            PersonCustomer person = new PersonCustomer(
                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("name"),
                    resultSet.getString("lastName"), resultSet.getString("identitynumber"),
                    resultSet.getString("nameoffather"), resultSet.getString("dateofbirth"));
            personList.add(person);
        }
        return personList;
    }

    public List<PersonCustomer> searchByIdentityNumber(String identityNumber) throws Exception {
        preparedStatement = connection.prepareStatement("select * from personcustomer WHERE identitynumber=?");
        preparedStatement.setString(1, identityNumber);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<PersonCustomer> personList = new ArrayList<PersonCustomer>();
        while (resultSet.next()) {
            PersonCustomer person = new PersonCustomer(
                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("name"),
                    resultSet.getString("lastName"), resultSet.getString("identitynumber"),
                    resultSet.getString("nameoffather"), resultSet.getString("dateofbirth"));
            personList.add(person);
        }
        return personList;
    }

    public List<PersonCustomer> searchById(int id) throws Exception {
        preparedStatement = connection.prepareStatement("select * from personcustomer WHERE id=?");
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<PersonCustomer> personList = new ArrayList<PersonCustomer>();
        while (resultSet.next()) {
            PersonCustomer person = new PersonCustomer(
                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("name"),
                    resultSet.getString("lastName"), resultSet.getString("identitynumber"),
                    resultSet.getString("nameoffather"), resultSet.getString("dateofbirth"));
            personList.add(person);
        }
        return personList;
    }


    public void delete(int id) throws Exception {
        preparedStatement = connection.prepareStatement("delete from personcustomer where id=?");
        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();
    }

    public void update(PersonCustomer person) throws Exception {

        System.out.println("in DA" + person.getName() + person.getLastName() + person.getIdentityNumber() + person.getNameOfFather() + person.getDateOfBirth() + person.getId());
        preparedStatement = connection.prepareStatement
                ("update dotin3.personcustomer set dotin3.personcustomer.name=?,dotin3.personcustomer.lastname=?,dotin3.personcustomer.identitynumber=?,dotin3.personcustomer.nameoffather=?,dotin3.personcustomer.dateofbirth=?where dotin3.personcustomer.id=?");
        preparedStatement.setString(1, person.getName());
        preparedStatement.setString(2, person.getLastName());
        preparedStatement.setString(3, person.getIdentityNumber());
        preparedStatement.setString(4, person.getNameOfFather());
        preparedStatement.setString(5, person.getDateOfBirth());
        preparedStatement.setInt(6, person.getId());
        preparedStatement.executeUpdate();
    }

    public void close() throws Exception {
        preparedStatement.close();
        connection.close();
    }
}

package model.repository;

import model.entity.CompanyCustomer;
import model.entity.PersonCustomer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Hi on 8/1/2020.
 */
public class CompanyCustomerDA {
    private PreparedStatement preparedStatement;
    private Connection connection;
    private int countCustomers = 0;
    private List<Integer> customernumbers = new ArrayList<Integer>();

    public CompanyCustomerDA() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        connection = DriverManager.getConnection("jdbc:mysql://localhost/dotin3?" + "user=sima&password=myjava123");

    }

    public void insert(CompanyCustomer companyCustomer) throws Exception {
        preparedStatement = connection.prepareStatement("insert into companycustomer" +
                " (customernumber,companyname,economicid,dateofregistering)" +
                " values (?,?,?,?)");
        countCustomers++;
        for (int i = 100; i < 1000; i++) {
            customernumbers.add(new Integer(i));
        }
        Collections.shuffle(customernumbers);
        preparedStatement.setString(1, String.valueOf(customernumbers.get(countCustomers)));
        preparedStatement.setString(2, companyCustomer.getCompanyName());
        preparedStatement.setString(3, companyCustomer.getEconomicId());
        preparedStatement.setString(4, companyCustomer.getDateOfRegistering());
        preparedStatement.executeUpdate();
    }

    public String selectCustomerNumber() throws Exception {
        preparedStatement = connection.prepareStatement("SELECT customernumber FROM dotin3.companycustomer ORDER BY id DESC LIMIT 0, 1\n");
        ResultSet resultSet = preparedStatement.executeQuery();
        String customerNumber = null;
        while (resultSet.next()) {
            customerNumber = resultSet.getString("customernumber");
        }
        return customerNumber;
    }

    public List<CompanyCustomer> select() throws Exception {
        preparedStatement = connection.prepareStatement("select * from companycustomer ");
        ResultSet resultSet = preparedStatement.executeQuery();
        List<CompanyCustomer> companyCustomers = new ArrayList<CompanyCustomer>();
        while (resultSet.next()) {
            CompanyCustomer companyCustomer = new CompanyCustomer(
                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("companyname"),
                    resultSet.getString("economicid"), resultSet.getString("dateofregistering"));
            companyCustomers.add(companyCustomer);
        }
        return companyCustomers;
    }

    public List<CompanyCustomer> searchByCompanyName(String companyname) throws Exception {
        preparedStatement = connection.prepareStatement("select * from dotin3.companycustomer WHERE companyname =?");
        preparedStatement.setString(1, companyname);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<CompanyCustomer> companyCustomers = new ArrayList<CompanyCustomer>();
        while (resultSet.next()) {
            CompanyCustomer companyCustomer = new CompanyCustomer(
                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("companyname"),
                    resultSet.getString("economicid"), resultSet.getString("dateofregistering"));
            companyCustomers.add(companyCustomer);
        }
        return companyCustomers;
    }

    public List<CompanyCustomer> searchByEconomicId(String economicId) throws Exception {
        preparedStatement = connection.prepareStatement("select * from companycustomer WHERE economicid =?");
        preparedStatement.setString(1, economicId);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<CompanyCustomer> companyCustomers = new ArrayList<CompanyCustomer>();
        while (resultSet.next()) {
            CompanyCustomer companyCustomer = new CompanyCustomer(
                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("companyname"),
                    resultSet.getString("economicid"), resultSet.getString("dateofregistering"));
            companyCustomers.add(companyCustomer);
        }
        return companyCustomers;
    }

    public List<CompanyCustomer> searchById(int id) throws Exception {
        preparedStatement = connection.prepareStatement("select * from companycustomer WHERE id =?");
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<CompanyCustomer> companyCustomers = new ArrayList<CompanyCustomer>();
        while (resultSet.next()) {
            CompanyCustomer companyCustomer = new CompanyCustomer(
                    resultSet.getInt("id"), resultSet.getString("customernumber"), resultSet.getString("companyname"),
                    resultSet.getString("economicid"), resultSet.getString("dateofregistering"));
            companyCustomers.add(companyCustomer);
        }
        return companyCustomers;
    }

    public void delete(int id) throws Exception {
        preparedStatement = connection.prepareStatement("delete from companycustomer where id=?");
        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();
    }

    public void update(CompanyCustomer companyCustomer) throws Exception {
        preparedStatement = connection.prepareStatement
                ("update companycustomer set companyname=?,economicid=?,dateofregistering=? where id=?");
        preparedStatement.setString(1, companyCustomer.getCompanyName());
        preparedStatement.setString(2, companyCustomer.getEconomicId());
        preparedStatement.setString(3, companyCustomer.getDateOfRegistering());
        preparedStatement.setInt(4, companyCustomer.getId());
        preparedStatement.executeUpdate();
    }

    public void close() throws Exception {
        preparedStatement.close();
        connection.close();
    }
}

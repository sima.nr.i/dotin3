package controller;

import model.entity.PersonCustomer;
import model.service.PersonCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hi on 8/3/2020.
 */
@WebServlet("/searchbyid.do")
public class SearchById extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            PersonCustomerService personCustomerService = new PersonCustomerService();
            req.setAttribute("list", personCustomerService.searchById(Integer.parseInt(req.getParameter("id"))));
            req.getRequestDispatcher("/result.jsp").forward(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

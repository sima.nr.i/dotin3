package controller;

import model.entity.CompanyCustomer;
import model.service.CompanyCustomerService;
import model.service.PersonCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hi on 8/3/2020.
 */
@WebServlet("/searchbycompanyname.do")
public class SearchByCompanyName extends HttpServlet {


    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            CompanyCustomerService companyCustomerService = new CompanyCustomerService();
            req.setAttribute("list2", companyCustomerService.searchByCompanyName(req.getParameter("companyName")));
            req.getRequestDispatcher("/result.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

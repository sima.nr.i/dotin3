package controller;

import model.entity.CompanyCustomer;
import model.entity.PersonCustomer;
import model.service.CompanyCustomerService;
import model.service.PersonCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/3/2020.
 */
@WebServlet("/updatecompanycustomer.do")
public class UpdateCompanyCustomer extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CompanyCustomerService companyCustomerService = new CompanyCustomerService();
        CompanyCustomer companyCustomer = new CompanyCustomer(Integer.parseInt(req.getParameter("companyId")), req.getParameter("companyName"), req.getParameter("economicId")
                , req.getParameter("dateOfRegistering"));
        try {
            companyCustomerService.update(companyCustomer);
            resp.sendRedirect("/findallcompanycustomer.do");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package controller;

import model.entity.PersonCustomer;
import model.service.PersonCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/1/2020.
 */
@WebServlet("/delete.do")
public class Delete extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            PersonCustomerService personCustomerService = new PersonCustomerService();
            personCustomerService.delete(Integer.parseInt(req.getParameter("id")));
            resp.sendRedirect("/findAll.do");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

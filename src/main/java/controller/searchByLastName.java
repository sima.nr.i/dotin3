package controller;

import model.service.PersonCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/2/2020.
 */
@WebServlet("/searchbylastname.do")
public class searchByLastName extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            PersonCustomerService personCustomerService = new PersonCustomerService();
            req.setAttribute("list", personCustomerService.searchByLastName(req.getParameter("lastName")));
            req.getRequestDispatcher("/result.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

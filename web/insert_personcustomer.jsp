<%--
  Created by IntelliJ IDEA.
  User: Hi
  Date: 8/9/2020
  Time: 5:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        /* Style the submit button */
        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        /* Add a background color to the submit button on mouse-over */
        input[type=submit]:hover {
            background-color: #45a049;
        }
    </style>
    <title>Title</title>
</head>
<body>
<form action="/save.do">
    <%--<input type="text" id="id" name="id" placeholder="ID">--%>
    <input type="text" id="name" name="name" placeholder="NAME">
    <input type="text" id="lastName" name="lastName" placeholder="LAST NAME">
    <input type="text" id="identityNumber" name="identityNumber" placeholder="IDENTITY NUMBER">
    <input type="text" id="nameOfFather" name="nameOfFather" placeholder="NAME OF FATHER">
    <input type="text" id="dateOfBirth" name="dateOfBirth" placeholder="DATE OF BIRTH">
    <input type="submit" value="register">
</form>
</body>
</html>

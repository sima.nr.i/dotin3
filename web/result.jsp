<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Hi
  Date: 8/2/2020
  Time: 5:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }


        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        /* Style the submit button */
        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        /* Add a background color to the submit button on mouse-over */
        input[type=submit]:hover {
            background-color: #45a049;
        }


    </style>
    <title>Title</title>
</head>
<body>

<table border="0" style="width: 100%" id="customers">
    <tr>
        <td>ID</td>
        <td>CUSTOMERNUMBER</td>
        <td>NAME</td>
        <td>FAMILY</td>
        <td>identityNumber</td>
        <td>nameOfFather</td>
        <td>dateOfBirth</td>
    </tr>
    <c:forEach items="${requestScope.list}" var="p">
        <%--<form action="/update.do">--%>
            <%--<tr>--%>
                <%--<td><input readonly type="text" name="id" value="${p.id}"/></td>--%>
                <%--<td><input type="text" name="name" value="${p.name}"/></td>--%>
                <%--<td><input type="text" name="family" value="${p.family}"/></td>--%>
                <%--<td><input type="submit" value="UPDATE"/></td>--%>
                <%--&lt;%&ndash;<td><input type="button" onclick="removePerson(${p.id})" value="DELETE"/></td>&ndash;%&gt;--%>
            <%--</tr>--%>
        <%--</form>--%>
        <%--<form action="/update.do">--%>

            <tr>
                <td><c:out value="${p.id}"/></td>
                <td><c:out value="${p.customerNumber}"/></td>
                <td><c:out value="${p.name}"/></td>
                <td><c:out value="${p.lastName}"/></td>
                <td><c:out value="${p.identityNumber}"/></td>
                <td><c:out value="${p.nameOfFather}"/></td>
                <td><c:out value="${p.dateOfBirth}"/></td>
                    <%--<td><input type="button" onclick="removePerson(${p.id})" value="DELETE"/></td>--%>
            </tr>
        <%--</form>--%>
    </c:forEach>
</table>

<form action="edit.jsp">
    <input type="submit" value="UPDATE">
</form>
<form action="delete.jsp">
    <input type="submit" value="DELETE">
</form>

<table border="0" style="width: 100%" id="customers">
    <tr>
        <td>ID</td>
        <td>CUSTOMERNUMBER</td>
        <td>COMPANYNAME</td>
        <td>ECONOMICID</td>
        <td>DATEOFREGISTERING</td>
    </tr>
    <c:forEach items="${requestScope.list2}" var="p">


        <tr>
            <td><c:out value="${p.id}"/></td>
            <td><c:out value="${p.customerNumber}"/></td>
            <td><c:out value="${p.companyName}"/></td>
            <td><c:out value="${p.economicId}"/></td>
            <td><c:out value="${p.dateOfRegistering}"/></td>
            </br>
                <%--<td><input type="button" onclick="removePerson(${p.id})" value="DELETE"/></td>--%>
        </tr>
        <%--</form>--%>
    </c:forEach>
</table>
<%--<form action="edit.jsp">--%>
    <%--<input type="submit" value="UPDATECOMPANYCUSTOMER">--%>
<%--</form>--%>
<%--<form action="delete.jsp">--%>
    <%--<input type="submit" value="DELETECOMPANYCUSTOMER">--%>
<%--</form>--%>

</body>
</html>

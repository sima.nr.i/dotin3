<%--
  Created by IntelliJ IDEA.
  User: Hi
  Date: 8/15/2020
  Time: 12:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        /* Style the submit button */
        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        /* Add a background color to the submit button on mouse-over */
        input[type=submit]:hover {
            background-color: #45a049;
        }
    </style>
    <title>Title</title>
</head>
<body>
<form action="/searchbyidcompanycustomer.do">
    <input type="text" name="companyId" placeholder="COMPANY ID">
    <input type="submit" value="SEARCH BY ID COMPANYCUSTOMER">
</form>
<form action="/searchbycompanyname.do">
    <input type="text" name="companyName" placeholder="COMPPANY NAME">
    <input type="submit" value="SEARCH BY COMPANYNAME">
</form>
<form action="/searchbyeconomicid.do">
    <input type="text" name="economicId" placeholder="ECONOMIC ID">
    <input type="submit" value="SEARCH BY ECONOMICID">
</form>
</body>
</html>

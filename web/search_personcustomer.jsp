<%--
  Created by IntelliJ IDEA.
  User: Hi
  Date: 8/9/2020
  Time: 5:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        /* Style the submit button */
        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        /* Add a background color to the submit button on mouse-over */
        input[type=submit]:hover {
            background-color: #45a049;
        }
    </style>
    <title>Title</title>
</head>
<body>
</br>
<form action="/searchbyid.do">
    <input type="text" name="id" placeholder="ID">
    <input type="submit" value="SEARCHBYID">
</form>
<form action="/searchbyname.do">
    <input type="text" name="name" placeholder="NAME">
    <input type="submit" value="SEARCHBYNAME">
</form>
<form action="/searchbylastname.do">
    <input type="text" name="lastName" placeholder="LAST NAME">
    <input type="submit" value="SEARCHBYLASTNAME">
</form>
<form action="/searchbyidentitynumber.do">
    <input type="text" name="identityNumber" placeholder="IDENTITY NUMBER">
    <input type="submit" value="SEARCHBYIDENTITY">
</form>
</body>
</html>
